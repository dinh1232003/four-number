import * as fs from 'fs';

const number = fs.readFileSync('number.txt','utf8')
const arrayLength:any = Number(number.split('\n')[0].split(' ')[0])
const sum:any = Number(number.split('\n')[0].split(' ')[1])
const arr:any = number.split('\n')[1].split(' ').map(Number)
const hashMap:any = new Map()
const keyMap:any = []
// console.log(arr)
for (let i = 0; i < arrayLength; i++) {
    for (let y = i + 1; y < arrayLength; y++) {
        hashMap.set(arr[i] + arr[y], [i, y])
        keyMap.push(arr[i] + arr[y])
        // console.log(hashMap.get(arr[i] + arr[y]))
    }
}

const fourValueSumX = ():boolean => {
    for (let i = 0; i < arrayLength; i++) {
        for (let y = i + 1; y < arrayLength; y++) {
            if (keyMap[i] + keyMap[y] == sum && notSameElement(keyMap[i], keyMap[y])) {
                console.log(hashMap.get(keyMap[i]) + " " + hashMap.get(keyMap[y]))
                return true
            }
        }
    }
    return false
}

const notSameElement = (key1: number, key2:number):boolean => {
    const value1 = hashMap.get(key1)
    const value2 = hashMap.get(key2)
    if (value1[0] == value2[0] || value1[0] == value2[1 || value1[1] == value2[0]] || value1[1] == value2[1]) return false
    return true
}

console.log(fourValueSumX())
